import React, { useState, useEffect } from 'react'
import ReactGA from 'react-ga';
import "./Invite.scss"
import Constants from "../utils/Constants"
import Converter from "../utils/Converter"

ReactGA.pageview(window.location.pathname + window.location.search);

// https://blog.cloudboost.io/execute-asynchronous-tasks-in-series-942b74697f9c

const Invite = (props) => {

    const [verse, setVerse] = useState('');
    const [helloWorld, setHelloWorld] = useState('');
    const [firstLine, setFirstLine] = useState('');
    const [names, setName] = useState('');
    const [secondLine, setSecondLine] = useState('');
    const [day, setDay] = useState('');
    const [date, setDate] = useState('');
    const [year, setYear] = useState('');
    const [time, setTime] = useState('');
    const [place, setPlace] = useState('');
    const [address, setAddress] = useState('');

    const verseString = "The LORD hath done great things for us; whereof we are glad."

    const helloWorldString = "Hello world!";
    const firstLineString = "\u0020 \u0020 \u0020 \u0020 \u0020 Their lives have been blessed by their love for each other and by their faith in the Lord!"
    const namesString = "Franklin and Sathya"
    const secondLineString = "together with their parents invite you to witness the vows that will join them as one,"
    const dayString = "on Friday,"
    const dateString = "the seventeenth of May,"
    const yearString = "two thousand nineteen,"
    const timeString = "at nine thirty in the morning,"
    const placeString = "CSI Home Church,"
    const addressString = "Nagercoil."


    useEffect(() => {
        Converter.convert();
        setVerse(verseString)
        const execute = async () => {
            await setText(helloWorldString, setHelloWorld);
            await setText(firstLineString, setFirstLine);
            await setText(namesString, setName)
            await setText(secondLineString, setSecondLine)
            await setText(dayString, setDay)
            await setText(dateString, setDate)
            await setText(yearString, setYear)
            await setText(timeString, setTime)
            await setText(placeString, setPlace)
            await setText(addressString, setAddress)
        }
        execute();
    }, []);

    // useEffect(() => {
    //     setVerse(verseString)
    //     setHelloWorld(helloWorldString)
    //     setFirstLine(firstLineString)
    //     setName(namesString)
    //     setSecondLine(secondLineString)
    //     setDay(dayString)
    //     setDate(dateString)
    //     setYear(yearString)
    //     setTime(timeString)
    //     setPlace(placeString)
    //     setAddress(addressString)
    // }, []);

    const logEvent = (category, action, value) => {
        ReactGA.event({
            category,
            action,
            value,
          });
    }


    const timer = (text, setter, timeout = 0) => {
        return new Promise(res => {
            if(timeout === 0) {
                setTimeout(res, 25)
                setter(text)
            } else {
                setTimeout(res, timeout)
            }
        });
    }

    const setText = async (text, setter) => {

        let t = '|';
        for (const c of text) {
            t =  t.slice(0, -1) + c + '|'
            await timer(t, setter);
        }

        await timer(null, null, 1000);

        t = t.replace('|', '')
        await timer(t, setter);

        return new Promise((resolve, reject) => {
            resolve('done');
        })
    }

    const doNothing = (e) => {
        e.preventDefault();
    }

    const next = e => {
        const urlParams = (new URL(document.location)).searchParams
        const queryPrams = urlParams.has('1') ? `?${urlParams}` : ''
        logEvent(Constants.ANALYTIC_CATEGORY_CLICK, Constants.ANALYTIC_ACTION_CLICK_NEXT, urlParams)
        props.history.push(`/event${queryPrams}`)
    }

    const launchPlayStore = () => {
        logEvent(Constants.ANALYTIC_CATEGORY_CLICK, Constants.ANALYTIC_ACTION_OPEN_PLAYSTORE, "")
        window.open("https://play.google.com/store/apps/details?id=com.daliya.invitation&autoVerify=true", "_blank")
    }

    return (
        <div className="parent">
            <div className="verse">
                {verse}
            </div>
            <div className="helloWorld">
                <div className="text">
                    <p>{helloWorld}</p>
                </div>
                <div className="android"
                    onClick={() => launchPlayStore()}>
                </div>            
                
            </div>
            <div className="firstLine">
                <p>{firstLine}</p>
            </div>
            <div className="names">
                <p>{names}</p>
            </div>
            <div className="secondLine">
                <p>{secondLine}</p>
            </div>
            <div className="venue">
                <div>
                    <p>{day}</p>
                </div>
                <div>
                    <p>{date}</p>
                </div>
                <div>
                    <p>{year}</p>
                </div>
                <div>
                    <p>{time}</p>
                </div>
                <div>
                    <p>{place}</p>
                </div>
                <div>
                    <p>{address}</p>
                </div>
            </div>
            <div
                className="next"
                onClick={e => next(e)}>
                =>
                </div>
        </div>
    )
}

export default Invite