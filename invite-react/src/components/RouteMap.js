import React from 'react'
import "./RouteMap.scss"
import Constants from "../utils/Constants";
import ReactGA from 'react-ga';


ReactGA.pageview(window.location.pathname + window.location.search);

const RouteMap = (props) => {

    const verse = "The LORD hath done great things for us; whereof we are glad."

    const next = e => {
        const urlParams = (new URL(document.location)).searchParams
        const queryPrams = urlParams.has('1') ? `?${urlParams}` : ''
        logEvent(Constants.ANALYTIC_CATEGORY_CLICK, Constants.ANALYTIC_ACTION_CLICK_NEXT, urlParams)
        props.history.push(`/visit${queryPrams}`)
    }
    const previous = e => {
        const urlParams = (new URL(document.location)).searchParams
        const queryPrams = urlParams.has('1') ? `?${urlParams}` : ''
        logEvent(Constants.ANALYTIC_CATEGORY_CLICK, Constants.ANALYTIC_ACTION_CLICK_PREVIOUS, urlParams)
        props.history.push(`/event${queryPrams}`)
    }

    const showMap = url => {
        logEvent(Constants.ANALYTIC_CATEGORY_CLICK, Constants.ANALYTIC_ACTION_OPEN_MAP, url)
        window.open(url);
    }

    const logEvent = (category, action, value) => {
        ReactGA.event({
            category,
            action,
            value,
          });
    }

    return (
        <div className="routeMapParent">
            <div className="verse">{verse}</div>
            <div className="route">

                <div className="source">
                    <div className="trivendaram">
                        <div className="trivendramAirport"
                            onClick={() => showMap('https://goo.gl/maps/ztocJ5wAVuH2')}>
                            Thiruvananthapuram Airport
                        </div>
                        <div className="line">
                            <div className="verticalLine"></div>
                            <div className="note">
                                15 minutes, by cab/auto/airportbus
                            </div>
                            <div className="verticalLine"></div>
                        </div>
                        <div className="trivendarmBustand"
                            onClick={() => showMap('https://goo.gl/maps/ZfzBnUaAeJ12')}>
                            Thiruvananthapuram (Thampanoor) Bus Stand
                        </div>
                    </div>

                    <div className="bangalore">
                        <div className="placeHolder"></div>
                        <div className="routeMap">
                            <span className="title">Route Map</span>
                            <span className="launchMap">(Click location to launch map)</span>
                        </div>
                        <div 
                            className="bangaloreLocation"
                            onClick={() => showMap('https://goo.gl/maps/ZjQVdnsbDcm')}>
                            Bangalore (Shanti Nagar)
                        </div>
                    </div>

                    <div className="chennai"
                        onClick={() => showMap('https://goo.gl/maps/dYJUhNBk6iP2')}>
                        <div>Chennai (Koyambedu)</div>
                    </div>
                </div>

                <div className="lineToVadaseri">
                    <div className="trivendramToVadaseri">
                        <div className="verticalLine"></div>
                        <div className="note">
                            2.30 Hours by bus
                        </div>
                        <div className="verticalLine"></div>
                    </div>

                    <div className="bangaloreToVadaseri">
                        <div className="verticalLine"></div>
                        <div className="note">
                            11 Hours by bus
                        </div>
                        <div className="verticalLine"></div>
                    </div>

                    <div className="chennaiToVadaseri">
                        <div className="verticalLine"></div>
                        <div className="note">
                            11 Hours by bus
                        </div>
                        <div className="verticalLine"></div>
                    </div>
                </div>

                
                <div className="vadaseri"
                    onClick={() => showMap('https://goo.gl/maps/wR2nNeCNBCt')}>
                    <div>
                        Nagercoil (Vadasery) Bus Stand
                    </div>
                </div>

                <div className="lineToDestination">
                    <div className="vadaseriToChruch">
                        <div className="verticalLine"></div>
                        <div className="note">
                            15 minutes, by cab/auto
                        </div>
                        <div className="verticalLine"></div>
                    </div>

                    <div className="vadaseriToSaral">
                        <div className="verticalLine"></div>
                        <div className="note">
                            45 minutes, by cab/auto
                        </div>
                        <div className="verticalLine"></div>
                    </div>
                </div>

                <div className="eventLocations">
                    <div 
                        className="homeChurch"
                        onClick={() => showMap('https://goo.gl/maps/ShQ5caqUWdp')}>
                        CSI HOME CHURCH (9.30 AM)
                    </div>
                    <div 
                        className="saral"
                        onClick={() => showMap('https://goo.gl/maps/sRhLMLunChE2')}>
                        Christ Arangam (6.30 PM)
                    </div>
                </div>
            </div>

            <div className="navigation">
                <div 
                    className="previous"
                    onClick={e => previous(e)}>
                    {"<="}
                </div>
                <div 
                    className="next"
                    onClick={e => next(e)}>
                    =>
                </div>
            </div>
        </div>
    )
}

export default RouteMap