import React, {useState, useEffect} from 'react'
import ReactGA from 'react-ga';
import "./Events.scss"
import Crypto from '../utils/Crypto'
import Constants from "../utils/Constants";

ReactGA.pageview(window.location.pathname + window.location.search);

const  Event = (props) => {

    const verse = "The LORD hath done great things for us; whereof we are glad."
    const [greeting, setGreeting ]= useState(null);

    const logEvent = (category, action, value) => {
        ReactGA.event({
            category,
            action,
            value,
          });
    }

    const showMap = (lat,lng) => {
        var url = "http://maps.google.com/maps?q=loc:" + lat + "," + lng;
        logEvent(Constants.ANALYTIC_CATEGORY_CLICK, Constants.ANALYTIC_ACTION_OPEN_MAP, url)
        window.open(url);
    }

    const next = e => {
        const urlParams = (new URL(document.location)).searchParams
        const queryPrams = urlParams.has('1') ? `?${urlParams}` : ''
        logEvent(Constants.ANALYTIC_CATEGORY_CLICK, Constants.ANALYTIC_ACTION_CLICK_NEXT, urlParams)
        props.history.push(`/route-map${queryPrams}`)
    }

    const previous = e => {
        const urlParams = (new URL(document.location)).searchParams
        const queryPrams = urlParams.has('1') ? `?${urlParams}` : ''
        logEvent(Constants.ANALYTIC_CATEGORY_CLICK, Constants.ANALYTIC_ACTION_CLICK_PREVIOUS, urlParams)
        props.history.push(`/invite${queryPrams}`)
    }

    useEffect(() => {
        const urlParams = (new URL(document.location)).searchParams
        const key = 'qf2nghbkrlh5copapj6iahjxjlbowh45'
        const iv = 'vynn54plasspls90'
        if(urlParams.has('0')){ // encrypt 
            const encrypted = Crypto.encrypt(key, iv, urlParams.get('0'))
        } else if (urlParams.has('1')) { // decrypt
            const decrypted = Crypto.decrypt(key, iv, urlParams.get('1'))
            setGreeting(decrypted)
        }
    }, []);

    return (
        <div className="eventsParent">
            <div className="verse">{verse}</div>

            <div className="greeting">{greeting}</div>
            
            <div className="events">Events</div>
            
            <div className="eventsContainer">
                <div className="wedding">
                    <div className="title">Holy Matrimony</div>
                    <div className="date">
                        <span className="dateLabel">Date: </span>
                        <span>17th May 2019</span>
                    </div>
                    <div className="place"
                        onClick={() => showMap(8.1840203, 77.425914)}>
                        <span className="placeLabel">Place: </span>
                        <span>CSI Home Church, Nagercoil</span>
                    </div>
                    <div className="time">
                        <span className="timeLabel">Time: </span>
                        <span>9.30AM</span>
                    </div>
                </div>
                <div className="reception">
                    <div className="title">Reception</div>
                    <div className="date">
                        <span className="dateLabel">Date: </span>
                        <span>17th May 2019</span>
                    </div>
                    <div className="place"
                        onClick={() => showMap(8.1717022, 77.3464751)}>
                        <span className="placeLabel">Place: </span>
                        <span>Christ Arangam, Saral</span>
                    </div>
                    <div className="time">
                        <span className="timeLabel">Time: </span>
                        <span>6.00PM</span>
                    </div>
                </div>
            </div>

            <div className="contacts">Contacts</div>

            <div className="contactsContaniner">
                <div className="contactsRow">
                    <div>
                        <p>Mr.Maria Michael</p>
                        <p>+91 9677059695</p>
                    </div>
                    <div>
                        <p>Mr.Benjamin</p>
                        <p>+91 8526482785</p>
                    </div>
                </div>
                <div className="contactsRow">
                    <div>
                        <p>Mr.Albert</p>
                        <p>+91 9442928865</p>
                    </div>
                    <div>
                        <p>Mr.Yesu Vedha Blessing</p>
                        <p>+91 8098665201</p>
                    </div>
                </div>
            </div>
            <div className="navigation">
                <div 
                    className="previous"
                    onClick={e => previous(e)}>
                    {"<="}
                </div>
                <div 
                    className="next"
                    onClick={e => next(e)}>
                    =>
                </div>
            </div>
        </div>
    )
}

export default Event