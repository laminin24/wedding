import React from 'react'
import ReactGA from 'react-ga';
import Constants from "../utils/Constants";
import "./Visit.scss"



ReactGA.pageview(window.location.pathname + window.location.search);

const Visit = (props) => {
    
    const verse = "The LORD hath done great things for us; whereof we are glad."

    const placesToVisit = [ 
        {
            "name": "Kanya Kumari beach",
            "nearbyPlaces": [ "Vivekananda Rock Memorial", "Thiruvalluvar Statue", "Mahatma Gandhi Mandapam", "Baywatch Park" ],
            "url": "https://goo.gl/maps/kfsw5GviPsj",
        },
        {
            "name": "Muttom beach",
            "nearbyPlaces": [],
            "url": "https://goo.gl/maps/7QPMirDQQ8s",

        },
        {
            "name": "Vattakottai Fort",
            "nearbyPlaces": [], 
            "url": "https://goo.gl/maps/dkqEuQcLvaF2",
        }, 
        {
            "name": "Padmanabhapuram Palace",
            "nearbyPlaces": [], 
            "url": "https://goo.gl/maps/U6Um4KeMPKQ2",
        }, 
        {
            "name": "Thirparappu Water Falls",
            "nearbyPlaces": [], 
            "url": "https://goo.gl/maps/PEWq3PWz2Y22",
        },
        {
            "name": "Mathur Aqueduct",
            "nearbyPlaces": [], 
            "url": "https://goo.gl/maps/goDyhgeCLGE2", 
        },
        {
            "name": "Courtallam Falls",
            "nearbyPlaces": ["Courtallam Main Falls", "Old Coutralam Falls", "Coutralam Five falls", "Tigar Falls", "Shenbaga Devi Waterfalls", "Chitraruvi"], 
            "url": "https://goo.gl/maps/BcQo8bHKA9N2",
        },
        {
            "name": "Sothavilai Beach",
            "nearbyPlaces": [], 
            "url": "https://goo.gl/maps/hy7fyEwJEXs",
        },
        {
            "name": "Ullakkai aruvi",
            "nearbyPlaces": [], 
            "url": "https://goo.gl/maps/6m9teM5quex",
        },
        {
            "name": "Want to get banana chips and other snacks?",
            "nearbyPlaces": ["Sri Sindhu Sweets"], 
            "url": "https://goo.gl/maps/5KPLTiqqiT52",
        }
    ];

    const previous = e => {
        const urlParams = (new URL(document.location)).searchParams
        const queryPrams = urlParams.has('1') ? `?${urlParams}` : ''
        logEvent(Constants.ANALYTIC_CATEGORY_CLICK, Constants.ANALYTIC_ACTION_CLICK_PREVIOUS, urlParams)
        props.history.push(`/route-map${queryPrams}`)
    }

    const showMap = (url) => {
        logEvent(Constants.ANALYTIC_CATEGORY_CLICK, Constants.ANALYTIC_ACTION_OPEN_MAP, url)
        window.open(url);
    }

    const createPlaces = () => {
        const placess = placesToVisit.map( place => {
            const { name, nearbyPlaces, url } = place
            return (
                <div 
                    className="place"
                    key={name}
                    onClick={(e) => showMap(url)}>
                    <div className="name">{name}</div>
                    <span className="nearbyPlaces">{nearbyPlaces.join(', ')}</span>
                    <div className="line"></div>
                </div>
            )
        });
        return placess
    }

    const logEvent = (category, action, value) => {
        ReactGA.event({
            category,
            action,
            value,
          });
    }

    return (
        <div className="visitParent">
            <div className="verse">{verse}</div>
            <div className="titleContainer">
                <span className="title">Nearby places to visit</span>
                <span className="note">(click place name to launch Goolge Map)</span>
            </div>
            <div className="placeContainer">
                {createPlaces()}
            </div>
            <div className="navigation">
                <div 
                    className="previous"
                    onClick={e => previous(e)}>
                    {"<="}
                </div>
            </div>
        </div>
    )
}

export default Visit