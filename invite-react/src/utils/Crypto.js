import CryptoJS from 'crypto-js';

const Crypto = {
    encrypt: (key, iv, plainText) => {
        const encrypted = CryptoJS.AES.encrypt(plainText, CryptoJS.enc.Utf8.parse(key), {
            iv: CryptoJS.enc.Utf8.parse(iv)
        });
        const cipherText = encrypted.ciphertext.toString(CryptoJS.enc.Hex);
        return cipherText;
    },
    decrypt: (key, iv, cipherText) => {
        const cipherParams = CryptoJS.lib.CipherParams.create({
            ciphertext: CryptoJS.enc.Hex.parse(cipherText)
        });

        const decrypted = CryptoJS.AES.decrypt (
            cipherParams,
            CryptoJS.enc.Utf8.parse(key),
            { iv: CryptoJS.enc.Utf8.parse(iv) }
        );
        const plainText = decrypted.toString(CryptoJS.enc.Utf8);
        return plainText;
    }
}

export default Crypto;