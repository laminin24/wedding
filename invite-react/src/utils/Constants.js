const Constants = {
    ANALYTIC_CATEGORY_CLICK: 'click',
    ANALYTIC_ACTION_OPEN_MAP: 'open_map',
    ANALYTIC_ACTION_CLICK_NEXT: 'next',
    ANALYTIC_ACTION_CLICK_PREVIOUS: 'previous',
    ANALYTIC_ACTION_OPEN_PLAYSTORE: 'open_playstore',
}

export default Constants