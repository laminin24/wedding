import Crypto from "../utils/Crypto"

const dmiNames = [
    "Dear Vinoth Raj & Family",
    "Dear Ravi & Family",
    "Dear Raja & Family",
    "Dear Mathi & Family",
    "Dear Michael & Family",
    "Dear Shunmuga Sundaram & Family",
    "Dear Sylvia Jennifer & Family",
    "Dear Nithya & Family",
    "Dear Nivetha & Family",
    "Dear Kishore & Family",
    "Dear Anish & Family",
    "Dear Krishna Sai & Family",
    "Dear Rajesh & Family",
    "Dear Shibin & Family",
    "Dear Ismail & Family",
    "Dear Kamal & Family",
    "Dear Nivas & Family",
    "Dear Srinath & Family",
    "Dear Sujitha & Family",
    "Dear Steffi & Family",
    "Dear Thanks Ramya & Family",
    "Dear Arun Normal & Family",
    "Dear Aravind & Family",
    "Dear Dilbar & Family",
    "Dear Goutham & Family",
    "Dear Kamalesh & Family",
    "Dear Monisha & Family",
    "Dear Mary HJosepin & Family",
    "Dear Ruby Mam & Family",
    "Dear Nithin & Family",
];


const ktpcNames = [
    "Dear Anusuthan & Family",
    "Dear Suresh & Family",
    "Dear Basker & Family",
    "Dear Arumugam & Family",
    "Dear Army Selva Jet & Family",
    "Dear Genil & Family",
    "Dear Kamal & Family",
    "Dear Kesavan & Family",
    "Dear Krishna Raja & Family",
    "Dear Pravin & Family",
    "Dear Senthil & Family",
    "Dear Thivin  & Family",
    "Dear Anish & Family",
    "Dear Ayyappan & Family",
    "Dear P V Vijykumar & Family",
];


const schoolNames = [
    "Dear Anto & Family",
    "Dear Arul Rajan & Family",
    "Dear Biju & Family",
    "Dear Doni & Family",
    "Dear Hubert & Family",
    "Dear Jeni Britto & Family",
    "Dear Ponraj & Family",
    "Dear Siva Sankar & Family",
    "Dear Jaleen & Family",
    "Dear Jaghathees & Family",
    "Dear Milani & Family",
    "Dear Selva Kumar & Family",
    "Dear Sinoj & Family",
    "Dear Subash & Family",
    "Dear Theepan & Family",
    "Dear Eben Issac & Family",
    "Dear Jaleen  & Family",
    "Dear Jose Ajin & Family",
]


const payuNames = [
    "Dear Amit & Family", 
    "Dear Piyush K & Family", 
    "Dear Piyush J & Family", 
    "Dear Surya & Family", 
    "Dear Minie & Family", 
    "Dear Minish & Family", 
    "Dear Rahul & Family", 
    "Dear Shadab & Family", 
    "Dear Shoaib & Family", 
    "Dear Vipin & Family", 
    "Dear Umang & Family", 
    "Dear Aashis & Family", 
    "Dear Jitendar & Family", 
    "Dear Megha & Family", 
    "Dear Harjeet & Family", 
    "Dear Kunal  & Family", 
    "Dear Rohit  & Family", 
    "Dear Aparna & Family", 
    "Dear Srinivas & Family", 
    "Dear Guruchetan  & Family", 
    "Dear Shantanu & Family", 
    "Dear Sandeep & Family", 
    "Dear Prakesh & Family", 
    "Dear Rishab & Family", 
    "Dear Bhuvaneshwari & Family", 
    "Dear Ankit  & Family", 
    "Dear Sushil & Family", 
    "Dear Harmeet & Family", 
]

const hdfcNames = [
    "Dear Aizaz Rafiqi & Family",
    "Dear Akash Singhi & Family",
    "Dear Akashdeep Kundu & Family",
    "Dear Anand Mohan & Family",
    "Dear Anhad Mathur & Family",
    "Dear Anubhab Goel & Family",
    "Dear Arijit Saha & Family",
    "Dear Ashish Surve & Family",
    "Dear Dhananjay & Family",
    "Dear Gaurav Singh & Family",
    "Dear Gauraw Mishra & Family",
    "Dear Harshit Jain & Family",
    "Dear Himanshu Sharma & Family",
    "Dear Hitesh Singh & Family",
    "Dear Hozefa Ayyajiwala & Family",
    "Dear Indrajeet Ambadekar & Family",
    "Dear Jitendra Gorle & Family",
    "Dear Kalpesh Gnaekar & Family",
    "Dear Karan Batheja & Family",
    "Dear Karthik Goparaju & Family",
    "Dear Lokesh Joshi & Family",
    "Dear Mayank Garg & Family",
    "Dear Mehul Agarwal & Family",
    "Dear Milind Nayse & Family",
    "Dear Nadeem Shaikh & Family",
    "Dear Narendra Ganpule & Family",
    "Dear Neeta Sriwastva & Family",
    "Dear Nilesh Benke & Family",
    "Dear Pankaj Kumar & Family",
    "Dear Priyesh Suryavanshi & Family",
    "Dear Radhika Wagh & Family",
    "Dear Rahul Kotian & Family",
    "Dear Rajat Rawat & Family",
    "Dear Rajeshwar Singh & Family",
    "Dear Ritesh Vishwakarma & Family",
    "Dear Rupesh & Family",
    "Dear Sachin Kavade & Family",
    "Dear Sachin Talekar & Family",
    "Dear Sager Rana & Family",
    "Dear Sai Pavan Kumar Maddipoti & Family",
    "Dear Sanjay Jain & Family",
    "Dear Shivam Aggarwal/Singh & Family",
    "Dear Siddharth & Family",
    "Dear Sohel & Family",
    "Dear Suyash Jadhav & Family",
    "Dear Swapnil Chaudhari & Family",
    "Dear Swastik Shrivastava & Family",
    "Dear Tanvi Baranwal & Family",
    "Dear Vibhas Bhingarde & Family",
    "Dear Utkarsh Deep & Family",
    "Dear Vinisha & Family",
    "Dear Yuvai Hole & Family",
    "Dear Jaya Srikar & Family",
    "Dear Nishit & Family",
    "Dear Pramit & Family",
    "Dear Nikhil & Family",
    "Dear Ruben & Family",
    "Dear Sanjana & Family",
    "Dear Payal & Family",
    "Dear Kunal & Family",
    "Dear Aslam Khan & Family",
    "Dear Krishna Chaitanya & Family",
    "Dear Krishna & Family",
    "Dear Mahesh & Family",
    "Dear Mandeep & Family",
    "Dear Nikhil  & Family",
    "Dear Pardha & Family",
    "Dear Payal Dua & Family",
    "Dear Praful Kalbagwar & Family",
    "Dear Rupesh & Family",
    "Dear Swapnil  & Family",
    "Dear Sujoy & Family",
]



const others = [
    "Dear Raj & Family", 
    "Dear Muthu Vel Murugan & Family", 
    "Dear Ashwini & Family", 
    "Dear Abraham & Family", 
    "Dear Asheesh Minhas & Family", 
    "Dear Babu & Family", 
    "Dear Baby Sharmila & Family", 
    "Dear Chelladurai Paster & Family", 
    "Dear Edison Paster & Family", 
    "Dear Gijo & Family", 
    "Dear Raju & Family", 
    "Dear Sahadevan & Family", 
    "Dear Sandeep & Family", 
    "Dear Sharona & Family", 
    "Dear Manoj Calvin & Family", 
]

const specials = [
    // "Dear Prashant Poojary & Family", 
    // "Dear Prashant & Family",
    // "Dear Krishna Moorthi & Family", 
    // "Dear Anand Babu & Family",
    // "Dear Chitranjan & Family",
    // "Dear Ashok & Family",
    // "Dear Elango & Family",
    // "Dear Gilbert & Family",
    // "Dear Rakesh & Family",
    // "Dear Uday & Family",
    // "Dear Vaasan & Family",
    // "Dear Nipul & Family",
    // "Dear Shashi & Family",
    // "Dear Mukund & Family",
    // "Dear Divya mam & Family",
    // "Dear Ujjal & Family",
    // "Dear Sarath & Family",
    // "Dear Hemant & Family",
    // "Dear Dwarka Tiwari & Family",
    // "Dear Shreyas & Family",
    // "Dear Advait Jayant", 
    // "Dear Tanuj Gupta",
    // "Dear Sajin & Family",
    // "Dear Ruban & Family"

// "Dear Anish & Family ", 
// "Dear Ansel & Family ", 
// "Dear Aron& Family ", 
// "Dear Chandran & Family ",
// "Dear Felix & Family ", 
// "Dear Jenish & Family ", 
// "Dear Lingesh & Family ", 
// "Dear Rajesh & Family ",

// "Dear Antony Manoj & Family", 
// "Dear Arun & Family",
// "Dear Ponnalkeppen & Family",
// "Dear Joseph & Family",
// "Dear Sivanambi Rajan & Family",
// "Dear Daison & Family",
// "Dear Dilbar & Family",
// "Dear Lokesh Kumar & Family",
// "Dear Lokeshwaran & Family",
// "Dear Prabakaran & Family",
"Dear Rakesh & Family",

    
]

const convert = (array) => {
    const key = 'qf2nghbkrlh5copapj6iahjxjlbowh45'
    const iv = 'vynn54plasspls90'
    return array.map(name => {
        return `${name} ==>  https://invite.daliya.in/?1=${Crypto.encrypt(key, iv, name)}`
    });
}

const Converter = {
    convert: () => {
        // console.log(convert(dmiNames))
        // console.log(convert(ktpcNames))
        // console.log(convert(schoolNames))
        // console.log(convert(payuNames))
        // console.log(convert(hdfcNames))
        // console.log(convert(others))
        console.log(convert(specials))
    }
}

export default Converter;