import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import './App.scss';
import Events from './components/Events'
import Invite from './components/Invite'
import RouteMap from './components/RouteMap'
import Visit from './components/Visit'
import ReactGA from 'react-ga';

class App extends Component {

  componentDidMount() {
    const urlParams = (new URL(document.location)).searchParams
    const queryPrams = urlParams.has('1') ? `?${urlParams}` : ''
    ReactGA.initialize('UA-139195410-1');
    ReactGA.set({ userId: queryPrams });
  }

  render() {
    return (
      <Router>
        <div className="App">
          <Route path="/" exact component={Invite} />
          <Route path="/event/" component={Events} />
          <Route path="/invite/" component={Invite} />
          <Route path="/route-map/" component={RouteMap} />
          <Route path="/visit/" component={Visit} />
        </div>
      </Router>
    );
  }
}


export default App;
