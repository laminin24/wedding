package com.laminin.invite.feature

import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import kotlinx.android.synthetic.main.activity_main.*
import android.os.Vibrator
import android.os.VibrationEffect
import android.os.Build
import android.view.View
import android.content.Intent
import kotlinx.android.synthetic.main.activity_main_backup.*


class MainActivityBackup : AppCompatActivity() {

    var verse = "The LORD hath done great things for us; whereof we are glad."

    var inviteText = "Hello world! ||\n\n" +
            "\t\tTheir lives have been blessed by their love for each other| and by their faith in the Lord! ||" +
            "\n\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\txxxxxxxx and xxxxxxxxx\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t||\n\ntogether with their parents| invite you to witness the vows| that will join them as one on Monday,| " +
            "the fifteenth of May| two thousand nineteen| at ten o'clock in the morning,| CSI church,| Nagercoil.|"


    /*var inviteText = "Kanya|kumari!||\n\n\t\t\t\t\t\t\t\t\t\t\t\tKanyakumari is a coastal town in the state of Tamil Nadu on India's southern tip.||| Jutting into the Laccadive Sea, the town was known as Cape Comorin during British rule and is popular for watching sunrise and sunset over the ocean.||||| It's also a noted pilgrimage site thanks to its Bagavathi Amman Temple,||| dedicated to a consort of Shiva,||| and its Our Lady of Ransom Church,||| a center of Indian Catholicism.||||"*/

    val charArray = inviteText.toCharArray()

    val handler = Handler()
    var i = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_backup)
        handler.post(runnableTask)
        bodyEditText.setOnTouchListener { _, _ ->  true }
        previousDayAtBrides.setOnClickListener {
            val gmmIntentUri = Uri.parse("https://plus.codes/6JWV58CX+MF")
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapIntent)
        }
    }

    private val runnableTask = object : Runnable {
        override fun run() {
            if(i == 0) {
                bodyEditText.visibility = View.VISIBLE
            }
            if(charArray.size > i) {
                val c = charArray[i].toString()
                i += 1
                if(c.contentEquals("|")){
                    handler.postDelayed(this, 2500)
                } else {
                    bodyEditText.append(c)
                    vibrate()
                    handler.postDelayed(this, 100)
                }
            } else if (charArray.size == i) {
                bodyEditText.setText("")
                bodyEditText.visibility = View.GONE
                contactLayout.visibility = View.VISIBLE
            }

        }
    }

    private fun vibrate() {
        if (Build.VERSION.SDK_INT >= 26) {
            (getSystemService(VIBRATOR_SERVICE) as Vibrator).vibrate(VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE))
        } else {
            (getSystemService(VIBRATOR_SERVICE) as Vibrator).vibrate(50)
        }
    }


    val originalText = "Their lives have been blessed\n" +
            "by their love for each other\n" +
            "and by their faith in the Lord \n" +
            "Lisbeth Sophia Garner\n" +
            "and\n" +
            "Ryan Gabriel Grant\n" +
            "together with their parents\n" +
            "invite you to witness the vows\n" +
            "that will join them as one\n" +
            "on Saturday, the eighteenth of August\n" +
            "two thousand eighteen \n" +
            "at three o'clock in the afternoon\n" +
            "Oak Creek Community Church\n" +
            "4609 Grape Road\n" +
            "Mishawaka, Indiana"
}
