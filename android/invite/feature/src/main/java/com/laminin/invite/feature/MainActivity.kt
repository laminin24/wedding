package com.laminin.invite.feature


import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import kotlinx.android.synthetic.main.activity_main.*
import android.os.Vibrator
import android.os.VibrationEffect
import android.os.Build
import android.view.View
import android.content.Intent
import android.support.v4.app.Fragment




class MainActivity : AppCompatActivity(), OnFragmentInteractionListener {
    override fun openInviteFragment() {
        replaceFragment(InviteFragment.newInstance("", ""))
    }

    override fun openEventsFragment() {
        replaceFragment(EventsFragment.newInstance("", ""))
    }

    override fun openRouteMapFragment() {
        replaceFragment(RouteMapFragment.newInstance("", ""))
    }

    override fun openVisitFragment() {
        replaceFragment(VisitFragment.newInstance("", ""))
    }

    override fun openPrevious() {
        supportFragmentManager.popBackStack()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // However, if we're being restored from a previous state,
        // then we don't need to do anything and should return or else
        // we could end up with overlapping fragments.
        if (savedInstanceState != null) {
            return
        } else {
            addFragment(InviteFragment.newInstance("", ""))
//             addFragment(VisitFragment.newInstance("", ""))
        }
    }

    private fun addFragment(fragment: Fragment) {
        // Add the fragment to the 'fragment_container' FrameLayout
        supportFragmentManager.beginTransaction()
                .add(R.id.fragment_container, fragment).commit()

    }

    private fun replaceFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction.replace(R.id.fragment_container, fragment)
        transaction.addToBackStack(null)
        // Commit the transaction
        transaction.commit()
    }
}
