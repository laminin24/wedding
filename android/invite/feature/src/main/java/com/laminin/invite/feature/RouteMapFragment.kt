package com.laminin.invite.feature

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_route_map.*
import java.lang.Exception


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [RouteMapFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [RouteMapFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class RouteMapFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_route_map, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        nextMap.setOnClickListener { listener?.openVisitFragment() }
        previousMap.setOnClickListener { listener?.openPrevious() }
        triventdarmAirport.setOnClickListener { launchMap("https://goo.gl/maps/ztocJ5wAVuH2") }
        trivendramBusStand.setOnClickListener { launchMap("https://goo.gl/maps/ZfzBnUaAeJ12") }
        bangaloreBusStand.setOnClickListener { launchMap("https://goo.gl/maps/ZjQVdnsbDcm") }
        chennaiBusStand.setOnClickListener { launchMap("https://goo.gl/maps/dYJUhNBk6iP2") }
        vadaseryBusStand.setOnClickListener { launchMap("https://goo.gl/maps/wR2nNeCNBCt") }
        csiHomeChurch.setOnClickListener { launchMap("https://goo.gl/maps/ShQ5caqUWdp") }
        christArangam.setOnClickListener { launchMap("https://goo.gl/maps/sRhLMLunChE2") }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    fun launchMap(url: String) {
        try {
            val gmmIntentUri = Uri.parse(url)
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapIntent)
        } catch (e: Exception) {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            startActivity(browserIntent)
        }
    }



    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment RouteMapFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                RouteMapFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
