package com.laminin.invite.feature

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_visit.*
import android.support.v7.widget.LinearLayoutManager
import android.content.Intent.ACTION_VIEW
import android.util.Log
import java.lang.Exception


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [VisitFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [VisitFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class VisitFragment : Fragment(), RvAdapter.ClickListener {

    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    private val names = arrayListOf("Kanya Kumari beach", "Muttom beach", "Vattakottai Fort", "Padmanabhapuram Palace", "Thirparappu Water Falls", "Mathur Aqueduct", "Courtallam Falls", "Sothavilai Beach", "Ullakkai aruvi")
    private val nearbyPlaces = arrayListOf("Vivekananda Rock Memorial, Thiruvalluvar Statue, Mahatma Gandhi Mandapam, Baywatch Park", "", "", "", "", "", "Courtallam Main Falls, Old Coutralam Falls, Coutralam Five falls, Tigar Falls, Shenbaga Devi Waterfalls, Chitraruvi", "", "")
    private val urls = arrayListOf("https://goo.gl/maps/kfsw5GviPsj", "https://goo.gl/maps/7QPMirDQQ8s", "https://goo.gl/maps/dkqEuQcLvaF2", "https://goo.gl/maps/U6Um4KeMPKQ2", "https://goo.gl/maps/PEWq3PWz2Y22", "https://goo.gl/maps/goDyhgeCLGE2", "https://goo.gl/maps/BcQo8bHKA9N2", "https://goo.gl/maps/hy7fyEwJEXs", "https://goo.gl/maps/6m9teM5quex")

    private val placesToVisitList = arrayListOf<PlacesToVisit>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

        names.forEachIndexed { i: Int, name: String ->
            placesToVisitList.add(PlacesToVisit(name, nearbyPlaces[i], urls[i]))
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_visit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        previous.setOnClickListener { listener?.openPrevious() }
        val rvAdapter = RvAdapter()
        rvAdapter.data = placesToVisitList
        rvAdapter.listener = this
        rvPlacesToVisit.adapter =  rvAdapter
        val mLayoutManager = LinearLayoutManager(activity)
        rvPlacesToVisit.layoutManager = mLayoutManager
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun launchMap(placesToVisit: PlacesToVisit) {
        try {
            val gmmIntentUri = Uri.parse(placesToVisit.url)
            val mapIntent = Intent(ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapIntent)
        } catch (e: Exception) {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(placesToVisit.url))
            startActivity(browserIntent)
        }
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment VisitFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                VisitFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
