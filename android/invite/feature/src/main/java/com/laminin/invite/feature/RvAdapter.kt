package com.laminin.invite.feature

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.row.view.*

class RvAdapter : RecyclerView.Adapter<RvAdapter.ViewHolder>(){

    var data: MutableList<PlacesToVisit> = mutableListOf()
        set(value) {
        field = value
        notifyDataSetChanged()
    }

    var listener: ClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.row, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.setOnClickListener { listener?.launchMap(data[position]) }
        holder.bind(data[position])
    }

    override fun getItemCount() = data.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: PlacesToVisit) {
            itemView.nameTextView.text = item.name
            itemView.nearByPlacesTextView.text = item.nearbyPlaces
        }
    }

    interface ClickListener {
        fun launchMap(placesToVisit: PlacesToVisit)
    }
}