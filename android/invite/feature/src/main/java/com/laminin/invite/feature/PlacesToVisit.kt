package com.laminin.invite.feature

data class PlacesToVisit (val name: String, var nearbyPlaces: String, var url: String)