package com.laminin.invite.feature

import android.content.Context
import android.os.*
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_invite.*
import java.util.concurrent.TimeUnit


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [InviteFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [InviteFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class InviteFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    private var helloWorldString = "Hello world!\u0000\u0000\u0000"
    private var firstLineString = "\u0020 \u0020 \u0020 \u0020 \u0020 Their lives have been blessed by their love for each other\u0000\u0000 and by their faith in the Lord!\u0000\u0000"
    private var namesString = "\u0000\u0000\u0000\u0000Franklin and Sathya\u0000\u0000\u0000\u0000"
    private var secondLineString = "\u0000\u0000together with their parents\u0000 invite you to witness the vows that will join them as one,\u0000\u0000"
    private var dayString = "on Friday,"
    private var dateString = "the seventeenth of May,"
    private var yearString = "two thousand nineteen,\u0000\u0000\u0000"
    private var timeString = "at nine thirty in the morning,"
    private var placeString = "CSI Home Church,"
    private var addressString = "Nagercoil."

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_invite, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        nextMap.setOnClickListener { listener?.openEventsFragment() }

        // TODO fix coming back text.

        val textArray  =arrayOf(helloWorldString, firstLineString, namesString, secondLineString, dayString, dateString, yearString, timeString, placeString, addressString)
        val viewArray = arrayOf(helloWorld, firstLine, name, secondLine, day, date, year, time, place, address)

        viewArray.forEach { it.setOnTouchListener { _, _ ->  true } }

        Observable.zip(Observable.fromIterable(textArray.asIterable()), Observable.fromIterable(viewArray.asIterable()), BiFunction { string: String, view: EditText ->
            Pair(string, view)
        }).flatMap { pair ->
            Observable.fromIterable(pair.first.asIterable()).map {char->
                Pair(char,pair.second)
            }

        }.concatMap {
            val d : Long = if(it.first == '\u0000' )  500 else  100
            Observable.just(it).delay(d, TimeUnit.MILLISECONDS)
        }.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { result ->
                    if(userVisibleHint && result.first != '\u0000') {
                        result.second.requestFocus()
                        result.second.append(result.first.toString())
                        vibrate()
                    }
                }
    }

    override fun onResume() {
        super.onResume()
        // when the back button is pressed, we should clear the previous texts,
        arrayOf(helloWorld, firstLine, name, secondLine, day, date, year, time, place, address).forEach { it?.text?.clear() }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment InviteFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                InviteFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }

    private fun vibrate() {
        if (Build.VERSION.SDK_INT >= 26) {
            (activity?.getSystemService(AppCompatActivity.VIBRATOR_SERVICE) as Vibrator).vibrate(VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE))
        } else {
            (activity?.getSystemService(AppCompatActivity.VIBRATOR_SERVICE) as Vibrator).vibrate(50)
        }
    }
}
